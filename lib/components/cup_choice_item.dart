import 'package:flutter/material.dart';

class CupChoiceItem extends StatefulWidget {
  const CupChoiceItem({
    super.key,
    required this.pandon,
    required this.currentMoney
  });

  final num pandon; // 100
  final num currentMoney; // 0

  @override
  State<CupChoiceItem> createState() => _CupChoiceItemState();
}

class _CupChoiceItemState extends State<CupChoiceItem> {
  bool _isOpen = false;
  String imgSrc= 'assets/cup_reverse.jpg';

  void _calculateStart(){
    if (!_isOpen){
      setState(() {
        _isOpen = true;
      });
    }
  }

  void _calculateImgSrc(){

    // 상자 깠을 때
    // 판돈이 현재돈보다 적을 때 : 꽝.img
    // 판돈이 현재돈이랑 같을 때 : 100.img
    // 판돈이 현재돈보다 많을 때 : 당첨.img

    // 상자 안 깠을 때 : 무조건 cup_reverse.jpg
    String tempImgSrc = '';
    if (_isOpen) {
      if (widget.pandon < widget.currentMoney){
        tempImgSrc = 'assets/losing_ticket.png';
      }else if (widget.pandon == widget.currentMoney){
        tempImgSrc = 'assets/100won.jpg';
      }else {
        tempImgSrc = 'assets/prize.jpg';
      }
    } else {
      tempImgSrc = 'assets/cup_reverse.jpg';
    }

    setState(() {
      imgSrc = tempImgSrc;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _calculateStart();
        _calculateImgSrc();
      },
      child: SizedBox(
        width: 100,
        height: 100,
        child: Image.asset(imgSrc),
      ),
    );
  }
}
