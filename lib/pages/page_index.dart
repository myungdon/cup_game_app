import 'package:cup_game_app/components/cup_choice_item.dart';
import 'package:flutter/material.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  bool _isStart = false; // 시작했니?
  num pandon = 100;
  List<num> result = [0, 100, 200];

  void _startGame() {
    setState(() {     // 바꼈다고 해주는 친구
      _isStart = true;
      result.shuffle();
    });
  }
  // 다시하기 로직 만드는 중
  // @override
  // void initSate(){
  //   super.initState();  // 여기까지가 기본
  //   setState(() {
  //     result.shuffle();
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('야바위'),
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    if (_isStart) {
      return SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              children: [
                CupChoiceItem(pandon: pandon, currentMoney: result[0]),
                CupChoiceItem(pandon: pandon, currentMoney: result[1]),
                CupChoiceItem(pandon: pandon, currentMoney: result[2]),
              ],
            ),
            Container(
              child: OutlinedButton(
                onPressed: () {
                  setState(() {
                    _isStart = false;
                  });
                },
                child: const Text('종료'),
              ),
            )
          ],
        ),
      );
    } else {
      return SingleChildScrollView(
        child: Center(
          child: OutlinedButton(
            onPressed: () {
              _startGame();
            },
            child: const Text('시작'),
          ),
        ),
      );
    }
  }
}

